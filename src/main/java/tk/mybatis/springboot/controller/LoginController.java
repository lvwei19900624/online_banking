package tk.mybatis.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.springboot.model.Account;
import tk.mybatis.springboot.model.Person;
import tk.mybatis.springboot.service.LoginService;
import tk.mybatis.springboot.util.JsonResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author liuzh
 * @since 2015-12-19 11:10
 */
@Controller
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private LoginService loginService;

    //跳转到密码修改页面
    @RequestMapping("/showPassword")
    public String showPassword(){
        return "/personInfo/password";
    }

    //跳转到个人信息页面
    @RequestMapping("/showPerson")
    public String showPerson(String id, Model model){
        model.addAttribute("id", id);
        return "/personInfo/info";
    }

    //跳转到余额查询页面
    @RequestMapping("/showBalance")
    public String showBalance(){
        return "/bank/balance";
    }

    //登录信息判断
    @RequestMapping(value="/login",method = RequestMethod.POST)
    @ResponseBody
    public JsonResult<Account> handleLogin(Account userInfo, HttpServletRequest request) {
        Account account=null;
        try {
            account=loginService.findByUser(userInfo);
            request.getSession().setAttribute("account",account);
            request.getSession().setAttribute("username",account.getUsername());
            request.getSession().setAttribute("role",account.getRole());
        }catch (Exception e){
            return new JsonResult<Account>(e);
        }
        return  new JsonResult<Account>(account);
    }

    //解锁
    @RequestMapping(value="/getLock")
    @ResponseBody
    public JsonResult<Account> getLock(HttpSession session,String password) {
        Account account=(Account)session.getAttribute("account");
        if(account.getPassword().equalsIgnoreCase(password)){
            return new JsonResult<>(account);
        }else {
            return new JsonResult<>(1,"密码错误");
        }
    }

    //获取用户名
    @RequestMapping(value="/getUsername")
    @ResponseBody
    public JsonResult<Account> getUsername(HttpSession session) {
        Account account=(Account)session.getAttribute("account");
        try {
            Account newAccount=loginService.findByUser(account);
            return new JsonResult<Account>(newAccount);
        }catch (Exception e){
            return new JsonResult<Account>(e);
        }
    }

    //修改密码
    @RequestMapping(value="/changePassword")
    @ResponseBody
    public JsonResult<Integer> changePassword(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute("account");
        if (account==null){
            return   new JsonResult<>(1,"用户未登录，请重新登录");
        }else {
            String oldPassword = (String) request.getParameter("oldPassword");
            if (account.getPassword().equalsIgnoreCase(oldPassword)) {
                String newPassword = (String) request.getParameter("newPassword");
                account.setPassword(newPassword);
                Integer n = loginService.changeUser(account);
                if (n == 1) {
                    return new JsonResult<>(0, "密码修改成功");
                } else {
                    return new JsonResult<>(1, "密码修改失败");
                }
            } else {
                return new JsonResult<>(1, "密码输入错误");
            }
        }
    }

    //获取个人信息
    @RequestMapping(value="/getUser")
    @ResponseBody
    public JsonResult<Person> getUser(HttpServletRequest request, String id) {
        Person person=new Person();
        person.setAccountid(id);
        try {
            Person p = loginService.findByPerson(person);
            return new JsonResult<Person>(p);
        }catch (Exception e){
            return  new JsonResult<Person>(e);
        }
    }

    //修改个人信息
    @RequestMapping(value="/updatePerson")
    @ResponseBody
    public JsonResult<Integer> updatePerson(Person person) {
        try {
            int i=loginService.updatePerson(person);
            return new JsonResult<Integer>(i);
        }catch (Exception e){
            return  new JsonResult<Integer>(e);
        }
    }

}

package tk.mybatis.springboot.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InitController {

    private static final Logger logger = LoggerFactory.getLogger(InitController.class);

    @RequestMapping(value = "/toIndex")
    public String toIndex() {
        return "index";
    }

    @RequestMapping(value = "/toLogin")
    public String toLogin() {
        logger.info("登录页");
        return "login";
    }
}


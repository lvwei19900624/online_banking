
package tk.mybatis.springboot.mapper;

import tk.mybatis.springboot.model.Account;
import tk.mybatis.springboot.util.MyMapper;

public interface AccountMapper extends MyMapper<Account> {
}

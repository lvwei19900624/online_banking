package tk.mybatis.springboot.mapper;


import tk.mybatis.springboot.model.Menu;
import tk.mybatis.springboot.util.MyMapper;

public interface MenuMapper extends MyMapper<Menu> {
}

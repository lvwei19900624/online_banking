
package tk.mybatis.springboot.mapper;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.springboot.model.Transaction;
import tk.mybatis.springboot.util.MyMapper;

import java.util.Date;
import java.util.List;

public interface TransactionMapper extends MyMapper<Transaction> {
     List<Transaction> listTransaction(@Param("type") String type, @Param("beginTime") Date beginTime,
                                       @Param("endTime") Date endTime, @Param("accountid") String accountid,
                                       @Param("otherid") String otherid);
}

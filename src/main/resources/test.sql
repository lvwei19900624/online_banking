﻿/*
Navicat MySQL Data Transfer

Source Server         : zql
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : fashion

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-03-25 16:50:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `balance` decimal(18,2) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL COMMENT '管理员和普通用户的区别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1', 'admin', '123456', '1230.00', '启用', '管理员');
INSERT INTO `account` VALUES ('2', 'zql', '123456', '15980.00', '启用 ', '普通用户');
INSERT INTO `account` VALUES ('3', 'qcc', '123456', '0.00', '冻结 ', '普通用户');
INSERT INTO `account` VALUES ('4', 'qinjianping', '123456', '1299.00', '启用 ', '普通用户');
INSERT INTO `account` VALUES ('5', 'qq', '123456', '123.00', '启用', '普通用户');
INSERT INTO `account` VALUES ('6', '芹菜菜', '123456', '200.00', '启用', '普通用户');
INSERT INTO `account` VALUES ('7', '11', '11', '11.00', '启用', '普通用户');
INSERT INTO `account` VALUES ('8', '小白', '123456', '100.00', '启用', '普通用户');

-- ----------------------------
-- Table structure for f_menu
-- ----------------------------
DROP TABLE IF EXISTS `f_menu`;
CREATE TABLE `f_menu` (
  `id` varchar(255) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `is_parent` int(20) DEFAULT NULL,
  `checked` int(20) DEFAULT NULL,
  `open` int(20) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `menutype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of f_menu
-- ----------------------------
INSERT INTO `f_menu` VALUES ('1', '银行业务', null, '', '0', '1', '0', '', '0');
INSERT INTO `f_menu` VALUES ('10', '账户信息', null, null, '0', '1', '0', null, '1');
INSERT INTO `f_menu` VALUES ('11', '所有账户', '10', '/person/showAllPerson', '1', '1', '1', 'icon-man', '1');
INSERT INTO `f_menu` VALUES ('12', '已冻结账户', '10', '/person/showStopPerson', '1', '1', '1', 'icon-man', '1');
INSERT INTO `f_menu` VALUES ('13', '已启用账户', '10', '/person/showOpenPerson', '1', '1', '1', 'icon-man', '1');
INSERT INTO `f_menu` VALUES ('14', '开户', '10', '/person/newPerson', '1', '1', '1', 'icon-add', '1');
INSERT INTO `f_menu` VALUES ('15', '密码设置', null, '/user/showPassword', '1', '1', '1', 'icon-lock', '1');
INSERT INTO `f_menu` VALUES ('2', '信息设置', null, '', '0', '1', '0', '', '0');
INSERT INTO `f_menu` VALUES ('3', '存款', '1', '/trans/showDeposit', '1', '1', '0', '', '0');
INSERT INTO `f_menu` VALUES ('4', '取款', '1', '/trans/showWithdrawals', '1', '1', '0', '', '0');
INSERT INTO `f_menu` VALUES ('5', '转账', '1', '/trans/showTransfer', '1', '1', '0', '', '0');
INSERT INTO `f_menu` VALUES ('6', '交易记录', '1', '/trans/showAllTrans', '1', '1', '0', '', '0');
INSERT INTO `f_menu` VALUES ('7', '个人信息', '2', '/user/showPerson', '1', '1', '1', null, '0');
INSERT INTO `f_menu` VALUES ('8', '密码设置', '2', '/user/showPassword', '1', '1', '1', null, '0');
INSERT INTO `f_menu` VALUES ('9', '余额查询', '1', '/user/showBalance', '1', '1', '1', null, '0');

-- ----------------------------
-- Table structure for personinfo
-- ----------------------------
DROP TABLE IF EXISTS `personinfo`;
CREATE TABLE `personinfo` (
  `id` varchar(255) NOT NULL,
  `accountid` varchar(255) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `age` int(4) DEFAULT NULL,
  `sex` varchar(8) DEFAULT NULL COMMENT '0为男，1为女',
  `address` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `cardid` varchar(255) DEFAULT NULL COMMENT '身份证',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of personinfo
-- ----------------------------
INSERT INTO `personinfo` VALUES ('11', '1', '管理员', '23', '1', '广州天河区', '12564784452', '431024199102148865');
INSERT INTO `personinfo` VALUES ('21', '2', '钟巧丽', '18', '0', '湖南宁乡', '14566778445', '457124200010014421');
INSERT INTO `personinfo` VALUES ('31', '3', '芹菜菜', '32', '1', '广州天河区', '0735-642757', '457124200001014422');
INSERT INTO `personinfo` VALUES ('41', '4', '秦建平', '18', '0', '广州天河区', '0735-642758', '457124200012018423');
INSERT INTO `personinfo` VALUES ('51', '5', 'QQ', '18', '1', '广州天河区', '0735-642734', '457124200010015425');
INSERT INTO `personinfo` VALUES ('61', '6', '亲亲', '18', '0', '广州天河区', '0735-642753', '457124200005014445');
INSERT INTO `personinfo` VALUES ('71', '7', '12', '23', '0', '广州天河区', '0735-642756', '457124199507013454');
INSERT INTO `personinfo` VALUES ('81', '8', '小白啊', '33', '1', '广州天河区', '0735-642778', '457124198510014463');

-- ----------------------------
-- Table structure for transaction
-- ----------------------------
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `id` varchar(255) NOT NULL,
  `accountid` varchar(255) DEFAULT NULL,
  `otherid` varchar(255) DEFAULT NULL,
  `tr_money` decimal(18,2) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transaction
-- ----------------------------
INSERT INTO `transaction` VALUES ('06a558520c604220b0162ed9809c318a', '4', '2', '200.00', '2018-03-23 22:44:30', '转账');
INSERT INTO `transaction` VALUES ('0ff84c3983bb40408f9dfa08746059a1', '1', '1', '100.00', '2018-03-21 22:08:06', '存款');
INSERT INTO `transaction` VALUES ('130448ee193f4d8f96c0c297566fab8f', '4', '2', '1200.00', '2018-03-23 22:44:21', '转账');
INSERT INTO `transaction` VALUES ('1d84fea076e14ff8a0147df952041800', '2', '2', '200.00', '2018-03-23 22:13:57', '存款');
INSERT INTO `transaction` VALUES ('20c2517569664fd8b37fa231eaf1c8c3', '2', '2', '100.00', '2018-03-23 22:35:39', '存款');
INSERT INTO `transaction` VALUES ('2e85f277f6604436a4f0b95b76a36860', '1', '2', '2500.00', '2018-03-21 22:48:53', '转账');
INSERT INTO `transaction` VALUES ('45795944f85b4d5fa010e0db899f2c0c', '4', '4', '1000.00', '2018-03-23 22:43:51', '存款');
INSERT INTO `transaction` VALUES ('46d0e1f1d7774c39a74652b3eddcebfc', '1', '2', '200.00', '2018-03-21 22:42:51', '转账');
INSERT INTO `transaction` VALUES ('46d19e26ab2541ca870e50395e8708f6', '1', '1', '1000.00', '2018-03-21 21:14:55', '存款');
INSERT INTO `transaction` VALUES ('46e83dab55404cd8a552da9c899c2c7e', '1', '1', '110.00', '2018-03-21 22:05:26', '存款');
INSERT INTO `transaction` VALUES ('569378f7d99f4e00975a2579a604b4bf', '1', '1', '200.00', '2018-03-21 22:23:37', '取款');
INSERT INTO `transaction` VALUES ('57acd872f9bf493ca49ba6657682aa5d', '4', '4', '10000.00', '2018-03-23 22:44:00', '存款');
INSERT INTO `transaction` VALUES ('5e0c5f739fb34f9090c98cc739438b4a', '1', '1', '100.00', '2018-03-21 22:49:28', '取款');
INSERT INTO `transaction` VALUES ('62f63da6772e455092be538774c5a241', '4', '2', '10000.00', '2018-03-23 22:44:26', '转账');
INSERT INTO `transaction` VALUES ('754f2685f3f0440ba0909910c5598e99', '1', '1', '1000.00', '2018-03-21 22:49:24', '存款');
INSERT INTO `transaction` VALUES ('7a22892c62094da59ba90228cc97934e', '4', '2', '1000.00', '2018-03-23 22:44:38', '转账');
INSERT INTO `transaction` VALUES ('7e7d577e27054446941ea396f5f64d51', '4', '4', '200.00', '2018-03-23 22:43:47', '存款');
INSERT INTO `transaction` VALUES ('8086988446cc4ef383ecd240a3a69f99', '4', '4', '3000.00', '2018-03-23 22:43:55', '存款');
INSERT INTO `transaction` VALUES ('83fd1cf4b50c4722992384527c491d01', '2', '2', '200.00', '2018-03-23 22:33:24', '存款');
INSERT INTO `transaction` VALUES ('87be7177699344eea1f603511d4f2d36', '1', '1', '100.00', '2018-03-21 22:03:07', '存款');
INSERT INTO `transaction` VALUES ('8c1612256089486bb564d35714f3588f', '4', '4', '200.00', '2018-03-23 22:44:11', '取款');
INSERT INTO `transaction` VALUES ('978dea899e0f4fa7b7aae7ce006cb044', '1', '1', '200.00', '2018-03-22 00:31:50', '存款');
INSERT INTO `transaction` VALUES ('982eac6930564dfb89973a80d0f8c4b5', '2', '2', '100.00', '2018-03-23 22:35:25', '存款');
INSERT INTO `transaction` VALUES ('9ed5509ae6ed40cfa9499d20463801cb', '2', '2', '100.00', '2018-03-23 22:38:29', '存款');
INSERT INTO `transaction` VALUES ('b64352aaea924383b5765aec33a0df75', '2', '2', '100.00', '2018-03-23 22:33:42', '存款');
INSERT INTO `transaction` VALUES ('bf223b5c2d3e49939cd1326b77981470', '2', '2', '100.00', '2018-03-23 22:36:01', '存款');
INSERT INTO `transaction` VALUES ('c7ae29d1b0334796adccd52511c88404', '1', '1', '600.00', '2018-03-21 21:18:17', '存款');
INSERT INTO `transaction` VALUES ('ce13b3208cca451f9cbe510cfdcb8dc2', '2', '1', '120.00', '2018-03-23 22:39:14', '转账');
INSERT INTO `transaction` VALUES ('e636b1bd74c8421ab9f6e0047238a7e4', '4', '4', '1.00', '2018-03-23 22:44:05', '取款');
INSERT INTO `transaction` VALUES ('f50b4bdf8ffd468e966a3a5a40877d08', '4', '2', '300.00', '2018-03-23 22:44:33', '转账');
INSERT INTO `transaction` VALUES ('f65ec6ae32dd49e4a2da7a8481f6a496', '1', '1', '1000.00', '2018-03-21 22:20:57', '存款');
INSERT INTO `transaction` VALUES ('fd1fd08e643a484aaab2ce7e84ff23b4', '2', '2', '200.00', '2018-03-23 22:38:42', '取款');
